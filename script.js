let parent = document.querySelector(".images-wrapper");
let active = document.querySelector(".active");

function activChild() {
    active.classList.remove("active");
    let next = active.nextElementSibling;
    if (next === null) {
        next = parent.children[0];
    }
    next.classList.add("active");
    active = next;
}
setInterval(activChild, 2000);